# Implementations of the Bancor Protocol

* [TokenYield.io](https://tokenyield.io/), an easy-to-use dashboard for identifying and comparing dividend-paying tokens on EOS, [has announced](https://twitter.com/TokenYield/status/1143720391275249664) that it will integrate Bancor, allowing users to instantly convert EOS tokens without leaving the app.
* Japan-based [Financie](https://financie.jp/) is a social network built with the Bancor Protocol, allowing users to support the skills and dreams of other users on the platform. Users support “heroes” by purchasing their “hero cards”, which are tradable in Financie's marketplace.
* EOS-based game [Asteroid Rush](https://cryptonews.com/news/blockchain-game-asteroid-rush-presale-launch-a-story-of-ar-i-3534.htm) uses the Bancor Protocol to make its in-game economy function like a real one. As a result, the market value of resources and goods sold at auction is determined by the correlation of supply and demand.

Source: [blog.bancor.network](https://blog.bancor.network/bancor-quarterly-progress-report-q2-2019-9ab9483e048c) and [blog.bancor.network](https://blog.bancor.network/bancor-quarterly-progress-report-q1-2019-2979e585e133)

# DCoW - Double Coincidence of Wants

BNT is the first smart token, and using it as a reserve makes the same economic sense as it did to use Ether, rather than Bitcoin, to power a smart contract blockchain. Tokens are used in all of these examples for mass-collaboration between the sponsors, supporters, developers and users toward a common goal they are all incentivized to achieve. 

However, the concept of an automated market maker integrated into a cryptocurrency which can issue and destroy tokens while also being traded on any online exchange — *is a novel concept*, and should be recognized as such.

The Bancor protocol issues tokens in exchange for the reserve token and destroys tokens when paying out reserve tokens, which is what enables the reserve to grow and shrink relative to the market cap.

*Everything Bancor can do for you on chain, you can do by yourself off chain.* Moreover, the whole point of the the Bancor protocol is to enable an automatic, decentralized, transparent, trustless and straightforward management of the reserve(s), by the smart token itself.

Just as deterministic supply may be considered an advantage for a currency, deterministic reserve management (i.e. pricing mechanism) seems to offer a trustless, fair and practical solution, which benefits end-users with a more stable medium of exchange and a reserve that is always available.

It’s not about “proving” the reserve exists, it’s about letting an autonomous and predictable smart-contract manage it, for the benefit of all token holders.

The whole idea behind Bancor is that the reserve is not for you to hold. It is not owned by you. It is owned collectively by the token holders. It functions as their common liquidity pool, which is also used to set the price, keeping a constant ratio between the value of the liquidity pool and the smart token market cap.

*If you used Bancor, your Bancor smart contract would have no knowledge of what is happening out there in the real world.* Except that it will know, thanks to the mix of buy/sell orders that are processed by it. Every buy and sell order is an (arguably superior) source of knowledge as to what is happening in the real world.

Source: [blog.bancor.network](https://blog.bancor.network/this-analysis-of-bancor-is-flawed-18ab8a000d43)