# Chainlink

In April 2020 Bancor announced that it would start using [Chainlink](https://chain.link/) price oracles in its V2 release. Through integration with Chainlink's oracles Bancor's liquidity pools will become free of the risk associated with impermanent loss for both stable and volatile tokens.

Source: [medium.com](https://medium.com/paradigm-fund/chainlink-collabs-with-tezos-bancor-casperlabs-irisnet-hyperdao-bloom-acala-network-and-eb1c27b13d07)

Date: April 2020

# Kyber Network

In December 2019 Bancor announced a partnership with [Kyber Network](https://kyber.network/), one of the biggest DeFi players. Kyber will be sourcing liquidity from Bancor liquidity pools. This partnership highlights the emerging trend of composability in the DeFi industry: future exchanges are moving away from perceiving competition as a zero-sum game. Instead, there will be various liquidity protocols that can be aggregated into one package, so that DeFi products can benefit from multiple liquidity sources.

Source: [twitter.com](https://twitter.com/BLOCKTVnews/status/1211631423196729344)

Date: December 2019

# EOS Nation

In April 2020 Bancor announced launching the Dynamic Reserve Ratio (DDR) feature on the EOS blockchain in partnership with [EOS Nation](https://eosnation.io/), one of the top block producers in the EOS ecosystem.

EOS Nation built a new Bancor-based service which allows anyone create a DRR liquid token with their own parameters. Those tokens are initially fully pegged but over time they steadily emit assets from the token’s reserve and send them to the token's issuer, as the reserve ratio is designed to gradually decrease.

Source:  [blog.bancor.network](https://blog.bancor.network/bancor-dynamic-reserve-ratio-drr-liquid-tokens-3c7c733fdfea)

Date: April 2020

# 1inch.Exchange

In January 2020 Bancor listed [1inch.Exchange](https://1inch.exchange/) as one of several interfaces where users can add & remove liquidity from Bancor pools, track profits and find the best-performing pools on the network.

Source: [blog.bancor.network](https://blog.bancor.network/how-to-stake-liquidity-earn-fees-on-bancor-bff8369274a1)

Date: January 2020

# Zerion & Formatic

In July 2019 Bancor announced its integration with [Fortmatic](https://fortmatic.com/). This allows users to trade cryptocurrency using Fortmatic's UI without needing to sign up or sign in to a Bancor user account or install any third-party browser extension or wallets.

Also, users on the Fortmatic platform can apply decentralized asset management solutions, such as [Zerion](https://zerion.io/), to transfer their Ethereum-based assets into the Bancor web wallet, and then convert using the Bancor platform’s interface.

Source: [medium.com](https://medium.com/fortmatic/unlocking-the-potential-in-defi-with-bancor-fortmatic-97315660f627)

Date: July 2019

# Dune Analytics

In April 2020 Bancor announced that its contracts would become indexed and queryable on [Dune Analytics](https://www.duneanalytics.com/). As a result, [Bancor's Dashboard](https://explore.duneanalytics.com/dashboard/bancor) on Dune allows users to view key Bancor metrics on Ethereum, such as: total network volume, total network fees, fees per token, number of traders.

Source:  [blog.bancor.network](https://blog.bancor.network/bancor-is-live-on-dune-analytics-bfcea325ae70)

Date: April 2020

# Enjin

In September 2017 Bancor announced partnership with [Enjin](https://enjin.io/), a blockchain-based platform for games and apps. The main Enjin token (ENJ) started using Bancor liquidity pool mechanism, thus becoming part of the Bancor network. As a result, 20 million ENJ (together with equivalent Bancor tokens) were stored as reserve in the ENJBNT liquidity pool.

More importantly, all game items created on the Enjin Platform gained the ability to tie into the Bancor network and benefit in a similar way to ENJ. This opens up liquidity channels from gaming items to BNT and other smart tokens on the Bancor network.

Source: [blog.enjin.io](https://blog.enjin.io/bancor-partnership)

Date: September 2017

# Jarvis Network

In January 2020 Bancor announced partnership with [Jarvis](https://jarvis.network/), a DeFi platform. As a result, the Jarvis token (JRT) started using Bancor liquidity pool mechanism, thus becoming part of the Bancor network.

Before partnering with Bancor, Jarvis had already participated in the [Uniswap](https://uniswap.org/) network, but made the choice to join Bancor as well because of several important features that Bancor offered while Uniswap did not, e.g. incentives related to Bancor's inflation, flexibility in setting fee policies and support for stablecoin-based pools.

Source:  [medium.com](https://medium.com/jarvis-network/staking-jrt-and-usdb-on-bancor-f66d90f5ae0e)

Date: January 2020

# Block.One

On the EOS blockchain, RAM is required by smart-contracts to store account information such as keys, balances, and contract state. 

EOS adopts a free-market approach to allocating scarce resources such as RAM. To facilitate that, the blockchain implements a system contract acting as a market maker, that allows users to buy RAM from the system and sell RAM back to the system in exchange for the blockchain's native token.

In July 2018, after consulting with the Bancor team, the creators of EOS announced  that had decided to use Bancor's liquidity pool as the algorithm used for this critically important system contract.

This does not make EOS RAM tokens part of the Bancor network. Instead, it is an example of utilizing the Bancor protocol for implementing a core functionality of a top-tier smart-contract platform.

Source: [medium.com](https://medium.com/@bytemaster/eosio-ram-market-bancor-algorithm-b8e8d4e20c73)

Date: July 2018



























