# The Ecosystem: Projects Utilizing the Bancor Network

* An Overview
  * What is the Bancor Ecosystem?
  * Who are the owners of the Ecosystem projects?
  * How are Ecosystem projects funded?
  * The Ecosystem's Statistics

* The Components
  * The Official Wallet
  * Third-party Wallets
  * Third-party Liquidity Portals
  * The Stable Version of BNT
  * The Cross-Blockchain Bridge
* Business Partnerships
  * Partnership with Enjin

# Defense Strategies: Handling of Critiques, Misconceptions & Emergencies

* How did Bancor handle critiques & misconceptions?
  * Allegations about fundamental flaws
  * Allegations about dangerous backdoors
* The July 2018 Hack
  * What went wrong and why?
  * How efficient was the Bancor team in handling the emergency?
  * Full transparency as a PR strategy
* Lessons Learnt: The Idealism vs Pragmatism Dilemma

# Bancor's Liquidity Pool as a DeFi Product

* Immediate Trades vs Delayed Trades
* Internal Pricing vs External Pricing
* Liquidity Pools vs Decentralized Investment Funds
* Predicted Price vs Actual Price

# The Planned Bancor V2 Upgrade

* Three Problems Limiting Bancor's Adoption
  1. Impermanent Loss
  2. Necessity to Use BNT
  3. Excessive Slippage
* The Solution Offered in V2



---

---

---

# The Ecosystem: Projects Utilizing the Bancor Network

### An Overview

#### What is the Bancor Ecosystem?

For the purpose of this report, we define the Bancor Ecosystem as a network of business initiatives bound together by their usage of BNT (i.e. the Bancor network token), or the Liquidity Pools Registry (i.e. the official smart-contract keeping track of the registered Liquidity Pools).

Consequently, projects using only the Bancor protocol without being in any way connected to the Bancor network are deemed to be outside the Ecosystem -  we regard them as Protocol Implementations.

It's worth noting that the existence of the Ecosystem is a vital part of Bancor, yet it's not something that was "promised" in the WP. Thus, the Ecosystem should be regarded as a tangible manifestation that theoretical mechanisms described in the WP have their practical and useful implementations.

#### Who are the owners of the Ecosystem projects?

A couple of the projects within the Ecosystem have been created by the Bancor team, while most of them are the creation of third-parties unrelated to Bancor. We've been informed by the Bancor team that ultimately Bancor would like all Ecosystem projects to be operated by third-parties, so that Bancor can move its focus and resources to protocol development.

#### How are Ecosystem projects funded?

Our investigation shows that the following two models are currently applied:

* projects are either sponsored by grants (denominated in BNT) awarded by Bancor,
* or they have their own business models based on fees and / or user subscriptions.

Our understanding is that grants are just a temporary measure aimed at incentivizing the Ecosystem's growth.

#### The Ecosystem's Statistics

Regarding usage statistics, Bancor's Progress Report (as of Q2 2019) states that:	

> The Bancor Protocol has been used by millions of users to seamlessly convert and contribute liquidity to tokens. With over **$1.5 billion** in asset conversions processed and more than **$11 million** in ERC20 and EOS-based tokens staked to the network, across **150+ liquidity providers**, the Bancor ecosystem is just getting started.

Regarding token statistics, the report states that:

> Twelve new tokens were added by users to the Bancor Network in Q2 2019, ranging from stablecoins to tokenized art. That brings the total number of live tokens to 148, creating more than 11,000 token pairs convertible across EOS & Ethereum.

The current count stands at almost 600 tokens (of which 90% are Ethereum-based and 10% are EOS-based). The complete list is available [here](https://support.bancor.network/hc/en-us/articles/360000894211-What-tokens-are-currently-supported-by-the-Bancor-wallet-).

### The Components

#### The Official Wallet

Bancor’s intensive work on cross-blockchain infrastructure paid off in Q1 2019, with the long-awaited release of the [Unified Wallet](https://www.bancor.network/). It's called *unified* for a reason - it offers a built-in bridge between the Ethereum and EOS blockchains, as described in the subsequent section.

![](https://miro.medium.com/max/3856/1*W4tZ-1bm6v5p8x40hxmsaQ.png)

The wallet's main features include:

- Seamless management of [500+ ERC20 & EOS tokens](https://support.bancor.network/hc/en-us/articles/360000894211-What-tokens-are-currently-supported-by-the-Bancor-wallet-) in one **non-custodial** wallet. The non-custodial aspect is crucial here, as it differentiates it from other wallets offering a similar functionality but in a custodial manner (e.g. Coinbase).
- A vast amount of supported tokens - the user can perform conversions between ERC20 and EOS tokens in a single click across 9700+ token pairs.
- Built-in bootstrapping of EOS accounts - the user receives a free EOS account and enough EOS resources (RAM, CPU & NET) to get started. What's more he or she can [manage those EOS resources](https://support.bancor.network/hc/en-us/articles/360021452831-How-to-buy-sell-RAM-and-stake-unstake-CPU-NET-in-the-Bancor-wallet) directly from their wallet.
- Built-in support for token airdrops - the user receives any airdropped ERC20 or EOS token directly to their wallet.
- Easy sign-up process - it can be done using a mobile phone or a Telegram, Messenger or WeChat account.
- A fully responsive web design, so the wallet works equally well on both desktop and mobile devices.

Thus, the wallet unifies tokens based on EOS and Ethereum and ensures constant convertibility between these two platforms, as it offers instant and on-chain conversions between EOS and Ethereum-based tokens.

We have tested the Unified Wallet, both its desktop and mobile version, and have not encountered any major issues - all the main features work as advertised.

#### Third-party Wallets

Apart from the Bancor's official wallet offering, the ecosystem features several third-party wallets supporting the BNT token, eg. [Freewallet](https://freewallet.org/bnt-wallet), [Trust Wallet](https://trustwalletapp.com/) (as described [here](https://medium.com/@trustwallet/trust-wallet-and-bancor-work-together-to-expand-decentralized-exchange-landscape-201ec9a9ab66)), and [many more](http://cryptoteacher.info/the-top-8-best-bancor-wallets-that-you-should-use-for-storing-bnt/).

Additionally, the following third-party wallets can be used to make token conversions on the EOS blockchain using the Bancor Network: [Scatter](https://get-scatter.com/), [eosNova](https://eosnova.io/?l=en), [Infinito](https://www.infinitowallet.io/), [TokenPocket](https://www.tokenpocket.pro/en/), [MeetOne](https://meet.one/). While all of these EOS wallets can convert between EOS-based tokens on Bancor, Scatter and a Web3 wallet (such as Metamask) are required to move BNT across blockchains via [BancorX](http://x.bancor.network/).

[This video](https://support.bancor.network/hc/en-us/articles/360019864211--Video-How-to-Convert-ETH-to-EOS-Using-BancorX) shows how to seamlessly convert ETH into EOS (via the BNT token), using Metamask, BancorX and Scatter. We have tested this process and managed to replicate without any issues the results presented in the video.

#### Third-party Liquidity Portals

Permissionless liquidity portals are crucial for the ecosystem, as they constitute front-end interfaces for accessing Bancor’s permissionless smart contracts deployed on Ethereum & EOS. Those contracts are the basis of the Ecosystem, as they facilitate both creating and contributing liquidity to Bancor pools.

Without the interfaces available in Liquidity Portals users would have to deal with Bancor smart-contracts directly, which requires a level of expertise beyond the capability of most non-tech-savvy users. Thus, the main goal of those liquidity portals is to offer a user-friendly UI that walks the user through the process of creating new liquidity pools or contributing liquidity to existing liquidity pools.

The main permissionless liquidity portals include:

* on the Ethereum side: [1inch.exchange](https://1inch.exchange/#/lending), [CoTrader](http://bancor.cotrader.com/), [Zerion](https://app.zerion.io/)
* and on the EOS side: [EOSNation](http://bancor.eosnation.io/).

Their advertised features include:

- Create a Bancor liquidity pool for any ERC20 token (or any EOS token) using BNT or USDB
- Customize liquidity provider fees on your pool
- Add or withdraw liquidity from an existing Bancor pool on Ethereum (or EOS)
- View liquidity pool data (e.g. fees, reserve ratio, depth etc.)
- Utilize the liquidity by trading multiple ERC20 tokens (or EOS tokens) on the Bancor Network

We have tested all the above liquidity portals and the results were positive: in terms of technical functionality they actually deliver what they advertise.

#### The Stable Version of BNT

The release of USDB - a stable token backed by Bancor’s Network Token (BNT) - was announced in September 2019. Its introduction was a major step forward, as it allowed Bancor Liquidity Pools to use a price-stable asset in the role of a universal reserve token.

USDB is based on [PEG’s Stabletoken System](https://github.com/zachdoty/peg-usdb/wiki/Two-Vault-Peg-Stabletoken-System), which - by applying a series of smart-contract and an oracle - allows users to utilize any participating ERC20 token as collateral for minting stable tokens associated with that particular ERC20 token.

The PEG team deployed a [USDB:BNT liquidity pool](https://www.bancor.network/token/USDB), connecting any pool created with USDB to the BNT pools. As a result, USDB allows liquidity providers to create or contribute to pools on Bancor with one stable asset (or more) in the reserves. For example, a liquidity pool with two stable assets in its reserves (e.g., DAI:USDB or USDT:USDB) should exhibit very little divergence in asset prices, thereby maximizing the profitability of fees generated from conversions.

In addition, USDB-based pools are automatically interoperable with the Bancor Network, meaning USDB and BNT-based pools can instantly trade with one another.

It's worth noting that as liquidity providers connect to Bancor through USDB-based pools, BNT is bought and staked as collateral, in order to [issue the USDB](https://github.com/zachdoty/peg-usdb/wiki/Two-Vault-Peg-Stabletoken-System) needed for such pools. In this way, USDB expands both the options available to liquidity providers as well as the utility of BNT.

And finally, USDB was never mentioned in the White Paper. Thus, we consider it as an extra layer of utility enhancing the Ecosystem.

#### The Cross-Blockchain Bridge

This is probably the most important aspect of the Ecosystem as it makes it truly unique in the blockchain space.

Bancor's liquidity protocol was initially built on the Ethereum blockchain, then it was replicated on the EOS blockchain (thus evolving into a cross-chain liquidity protocol), and as a final step a cross-blockchain bridge (called [BancorX](https://support.bancor.network/hc/en-us/sections/360003210119-BancorX-Cross-Chain-Bridge)) was created.

BancorX enables automated conversions between Ethereum-based and EOS-based assets without users having to deposit funds on an exchange and without the need for order-matching between buyers and sellers. 

BancorX has [its limitations](https://support.bancor.network/hc/en-us/articles/360021197811-What-are-cross-chain-conversion-limits-) and out of technical necessity involves some centralized server-based elements, but historically it is the first successful attempt to automate transfer of value between two blockchains and make it practically viable.

Again, this feature significantly exceeds the plans envisioned in the WP.

### Business Partnerships

#### Partnership with Enjin

[Enjin](https://enjin.io/) is the largest gaming community creation platform online with more than 18 million users and more than 60 million global views a month.

The partnership between Bancor and Enjin was announced back in 2017 and started with Enjin using Bancor to streamline the process of conversion between Enjin's game items. As a result, Enjin users gained the ability to convert in-game items and other tokens on the Bancor Network without leaving the Enjin app.

> Our end-goal is to allow thousands of game item types to become interchangeable and liquid across any games you play. This will create rich and complex value networks, all tied together by the Bancor Protocol. Game items will be supported by both material (ENJ) value and also be able to reflect their true intrinsic value in the marketplace. Gamers will be able to put their items up for exchange and won’t need to wait for a second user to be updating their bids on an order-book.

Source: [blog.enjincoin.io](https://blog.enjincoin.io/bancor-enjin-partner-and-pre-sale-is-extended-7b6982442d08)

Following that, in 2019 Enjin extended this cooperation to integrate Bancor’s token conversion service (along with two other similar services, i.e. Kyber and Changelly) with the Enjin Wallet and enable simple in-app trading of Ethereum and hundreds of supported ERC-20 tokens.

Enjin wallet integration was an important step for Bancor, because of Enjin's close partnership with Samsung regarding the novel keystore feature available on the Samsung Galaxy S10 device.

> The Samsung Galaxy S10 which was released on March 8, 2018 has excellent deep blockchain feature known as Samsung blockchain keystore. Leveraging a partnership between Samsung and Enjin coin, Enjin (EJN) wallet was added directly into the Samsung blockchain keystore.

As a result, millions of mobile phone users now have access to the BNT token in the Enjin wallet available on Samsung's Galaxy S10.

> While lovers of Bancor BNT continue wallow in success for the massive adoption handed to the blockchain network, it has been estimated that Bancor will be open to the 41 million users available on Samsung S10 Enjin wallet.

Source: [newslogical.com](https://newslogical.com/41-million-users-on-samsung-galaxy-s10-enjin-enj-wallet-can-now-make-exchange-via-bancor/)

# Defense Strategies: Handling of Critiques, Misconceptions & Emergencies

### How did Bancor handle critiques & misconceptions?

#### Allegations about fundamental flaws

The most comprehensive critique of Bancor was produced by Emin Gün Sirer and Phil Daian in their article entitled [Bancor is Flawed](https://hackingdistributed.com/2017/06/19/bancor-is-flawed/). The authors came up with a long list of accusations, including:

* "Bancor does not solve a real problem"
* "The Bancor token is an unnecessary overhead"
* "Bancor could be done off-chain without losing any functionality"
* "Bancor is useless as it always trails the market"
* "Bancor smart contract has no knowledge of what is happening out there in the real world"

And many more.

Their main argument against Bancor came down to the following accusation: "the system is inherently doomed to bleed its reserves". This shows that most of the critique was the result of a fundamental misunderstanding of the main concept, as "bleeding of reserves" doesn't make sense in the context Bancor's smart-contracts, so it never happens.

The Bancor team [replied to the critique](https://blog.bancor.network/this-analysis-of-bancor-is-flawed-18ab8a000d43) in a very detailed manner. All items on the long list of accusations were addressed one by one, mainly by identifying the underlying misconceptions.

This was very much needed, as the Bancor concept, though relatively simple as a construct, has many nuances and unexpected consequences that take time and effort to grasp. Thus, this reply has a great educational value and it perfectly complements the description contained in the WP.

#### Allegations about dangerous backdoors

Another critical article entitled [Bancor Unchained: All Your Token Are Belong To Us](https://medium.com/unchained-reports/bancor-unchained-all-your-token-are-belong-to-us-d6bb00871e86) was published by Udi Wertheimer. The author describes what he considers dangerous backdoors:

* All transactions using the BNT token can be disabled by the team at any time for any reason
* The team can issue new tokens at any time and destroy any tokens from any account, at any time

The author's final verdict was quite harsh:

> This puts unprecedented, and worse, **unexpected** power in the hands of the contract owners. (...) I have never seen a token as centralized as BNT, that puts so much power in the hands of so few.

What's interesting, having the luxury of hindsight we see that he accurately predicted the way a hacker would attack Bancor several months later.

> I trust that Bancor’s team won’t try to misuse this backdoor. However, having so much power concentrated centrally, creates **a potential single point of failure**. The keys held by the team **could be stolen** for example.

Again, the Bancor team produced [a very detailed and balanced response](https://blog.bancor.network/response-to-bancor-unchained-cdb3bd2ba505) to Udi Wertheimer's critique. The main line of defense implied that those "backdoors" were a conscious choice made by Bancor as a result of the lessons learnt from the infamous DAO debacle. 

> We believe it is the right choice for Bancor to have options for dealing with unexpected problems, rather than preventing ourselves from doing so in the name of “decentralization”. Ultimately, we are the ones you will look to in the event of a breach. We are also the ones you are putting your trust in when you contribute to this project.

Bancor explained that these security switches were designed to enable them to respond quickly, mitigate damages, and responsibly handle potential thievery, in case a security breach in the code is detected. Thus, they assumed that the community would prefer to trust them as custodians of the code than trust that the code they created is flawless.

### The July 2018 Hack

#### What went wrong and why?

On July 9, 2018, the Bancor network experienced a major breach. One of the Bancor network’s [accounts](https://etherscan.io/address/0x009bb5e9fcf28e5e601b7d0e9e821da6365d0a9c) was compromised. Attackers took control of a wallet that was later used to transfer 25k ETH (~$12.5M), 230M NPXS (~$1M) and 3.2M BNT (~$10M) to a personal account. However, the Bancor team was able to mitigate some damage and recovered around $10 million worth of their own BNT tokens.

It's worth noting that the stolen funds belonged to Bancor itself, so none of Bancor users were affected and no user wallets were compromised.

This is what happened: a wallet used to upgrade Bancor smart-contracts was compromised and subsequently it was used to do the damage described above. The compromised account was actually the original creator of the Bancor token contract.

Usually, the contract’s creator is also its owner. This *contract ownership model* is a common development pattern used by many smart contracts on the Ethereum network. A Bancor owner account works pretty much the same as an admin account on a computer: it has access to a number of critical and restricted functions that regular accounts don’t have access to.

Luckily for Bancor, the compromised account was no longer an owner of any critical contract. Still, the account was actively used to upgrade some Bancor-related companion contracts. Naturally, every contract the hacked account had access to was drained of funds. In particular, the attackers drained:

- The compromised account, stealing all ETH from it
- The network’s converter contracts, stealing ether (ETH), Pundi X tokens (NPXS), and Bancor tokens (BNT)
- The network’s reserve accounts

Thus, it was not a matter of a flaw in the code, but rather a matter of losing control of a private key. In order to execute the Bancor hack, attackers called functions of several smart contracts in the network. The attackers simply initiated a Bancor exchange procedure and authorized transfers of tokens from the compromised contracts to their own accounts. 

In short, there was no real vulnerability in any of Bancor’s smart contracts. What happened is the backdoor designed to handle emergencies backfired badly and as a result it actually created an emergency.

#### How efficient was the Bancor team in handling the emergency?

Undoubtedly, the reaction of the Bancor team was very swift and efficient. Once the theft was identified, they immediately halted the network's operations and froze the stolen BNT, limiting the damage to the Bancor ecosystem from the theft. In addition to freezing $10 million in BNT tokens, the company transferred ownership of any contracts from the hacked account to more secure owners.

The ability to freeze tokens was built into the Bancor Protocol and was meant to be used in an extreme situation. This allowed Bancor to effectively stop the thief from running away with the stolen tokens.

What's crucial, when the Bancor Network was taken offline following the security breach, Bancor wallet users could still access and transfer their funds at any time. Also, the network operations were resumed within a couple of days after the breach.

However, it was not possible to freeze the stolen ETH and NPXS tokens. In this case, the only recourse available by the Bancor team (and actually applied) was working with cryptocurrency exchanges to trace the stolen funds and make it more difficult for the thief to liquidate them.

Following the hack, Bancor decided to increase the security of their main contract by granting its ownership to a multi-signature contract. This measure ensures that no single account can get access to the whole network.

#### Full transparency as a PR strategy

From day one after the hack the Bancor team applied a strategy of full transparency about what had happened and about the nature of the weakness the hacker had used.

Clearly, the backdoor backfired and Bancor assumed full responsibility for the private key mismanagement that had originated the disaster. At that time they could not reveal any details of the ongoing investigation but promised to do so once it was complete.

However, Bancor remained adamant that the idea of implementing the backdoor was still valid. Soon after the hack, a comprehensive article entitled [Decentralization is a Journey, Not a Destination](https://blog.bancor.network/decentralization-is-a-journey-not-a-destination-263c38b53bd) was published by the Bancor team, in which they defended their approach, yet welcomed the discussion about the difficult trade-offs they were facing.

#### Lessons Learnt: The Idealism vs Pragmatism Dilemma

Following the 2018 hack, Bancor was criticized for failing to be truly decentralized:

> The fundamental idea behind a decentralized exchange is that the users hold their private keys. Hence, decentralized exchanges don’t get hacked. Bancor has claimed to be a decentralized exchange and strictly speaking it looks like one because user funds remained unaffected in this hack. But there are reasons to categorize it as a centralized exchange. (...) Bancor can freeze tokens and can essentially be hacked - **an idea considered absurd with decentralization**.

Source: [Naimish Savghvi- Bancor Hack: Is the Compromised Exchange Truly Decentralised?](https://coincrunch.in/2018/07/10/bancor-hack-is-the-compromised-exchange-truly-decentralised/)

Other commenters pointed out that the approach applied by Bancor is just a manifestation of an inevitable process that needs to place before the technology matures:

> While at first glance this scheme seems to be pretty secure, it’s basically the biggest Bancor vulnerability. The very functions that helped save all of the stolen BNT tokens could be used to destroy the entire network. Just think about it: all that hackers need to do is get a hold of two out of four account keys. The security and well-being of the entire network rely on four pieces of data being hidden well enough. And all we can do is trust Bancor to keep their data safe and not abuse their own power. The good news is that this vulnerability is probably the only one left in the network.

Source: [Mihail Sotnichek - Blockchain Vulnerabilities: Bancor Exchange Hack](https://www.apriorit.com/dev-blog/554-bancor-exchange-hack)

Similar view - this author also perceives the situation as a "necessary compromise" that's needed because no better tools are available:

> One major criticism of Bancor has been that the introduction of some safety hatches and upgradability tools (one of which has backfired, unfortunately) makes the system less decentralized.

> It’s definitely a valid point and has been discussed at length, but we should also ask ourselves, what are the alternatives? An immutable and governed-by-code world highly resonates with me, but unfortunately the people who are crafting it are imperfect, to say the least.

> In the traditional world, we apply so many pre- and post-production tools to mitigate our errors: [TDD](https://en.wikipedia.org/wiki/Test-driven_development), 100% code coverage by tests, 24/7 [APM](https://en.wikipedia.org/wiki/Application_performance_management), data aggregation and analysis, self-healing code, [blue/green deployment](https://martinfowler.com/bliki/BlueGreenDeployment.html) — just to name a few. Yet, as we all know, very few of these tools are actually available on Ethereum today (and unlikely will be available any time soon).

> It would seem to me, that Bancor’s approach is a **necessary compromise**. It’s not perfect — but being familiar with all the advances and improvements that the team have been working on — the alternative of a stagnated and obsoleted system would be much worse.

Source: [Leonid Beder - In Defense of the Bancor Safety Hatch](https://medium.com/orbs-network/in-defense-of-the-bancor-safety-hatch-3e6fd88df5e2)

Bancor's response emphasized that what makes a system actually resistant to monopoly of power is not technical decentralization but the ability to easily fork the network. 

> So long as the existence and use of emergency control functions are fully transparent to the world, and users have the ability to **easily fork** networks if they disagree with their governance structures or execution, networks remain decentralized and censorship-resistant — a tremendous leap forward from the systems that dominate our online and offline worlds today.

>  At this stage, we must remind ourselves that decentralization is not the *goal* of this movement. Rather, decentralization is a *tool* to remove monopoly power over financial (and other) services — where too often entrenched actors misalign interests.

> The possibility of **easy forks**, and the means to verify every network actor’s interactions on the blockchain, do a great deal towards keeping network administrators honest and user-focused. The opportunity for blockchain network and protocol creators is in keeping users by setting them free, not by locking them in.

Source: [blog.bancor.network](https://blog.bancor.network/decentralization-is-a-journey-not-a-destination-263c38b53bd)

Here are three main arguments used by Bancor to prove that it upholds the premise of decentralization while also being pragmatic about security: 

* **Database Transparency**: unlike many crypto exchanges (including decentralized ones), on Bancor, there are *no private order books or off-chain functions*. Every movement of tokens and every price calculation is recorded on-chain forever. This ensures that all activity on Bancor is completely transparent and auditable by anyone. If there is malfeasance by Bancor or any of its network participants, one can identify and verify it immediately.
* **Open Source**: Since the Bancor Protocol operates on open-source smart contracts, all code is publicly accessible, forkable, and heavily scrutinized by the community. Whether Bancor succeeds or fails, anyone can use the Bancor Protocol and all its codebase to build automated market-making functionality into their project or create a new liquidity network, using any ERC20 token as its network token.
* **Self-Sovereign Authentication**: Bancor Network users can create a Bancor Wallet as well as **use any other** Web3 wallet (like MetaMask) to convert their tokens using the protocol’s automated and continuous liquidity mechanism. In either case, *users always remain in control of their private keys*. Even if the Bancor Network goes offline, users can always access their wallets using their downloadable keystore file or their 12 word phrase.

Thus, when the Bancor network was taken offline following the security breach, Bancor wallet users could still access and transfer their funds at any time. And because Bancor neither holds nor has access to these user funds, they were never susceptible to theft. This is a tremendous indicator of decentralization as users are not dependent on the network’s operation to retain access to their assets.

# Bancor's Liquidity Pool as a DeFi Product

#### Immediate Trades vs Delayed Trades 

In its novel approach, Bancor enables trading without relying on the notion of Double Coincidence of Wants. In short, Bancor gets rid of the order book.

It does so by facilitating an immediate exchange of assets - when interacting with a Liquidity Pool you send N tokens of asset A and immediately get M tokens of asset B in return (where the value of N tokens of A roughly equals the value of M tokens of B).

Thus, to make a trade using a Liquidity Pool you need to accept the current exchange rate offered by the Liquidity Pool, or just walk away from the trade. In other words, you cannot place an order indicating your intention to make a trade in the future, when the price is more suitable for you.

Is this a flaw in Bancor's design? We consider it a feature, not a flaw. Bancor addresses the need for liquidity and liquidity only makes sense in case you want to execute a trade at the *current* market price.

On the other hand, if you want to delay your trade hoping for a better price, what you actually need is not liquidity, but a space where you can safely place your order awaiting for a better price, and an efficient order matching mechanism on top of it. Fortunately there are existing solutions (both centralized and decentralized) that deliver just that, whereas Bancor's aim is facilitating instant trading.

#### Internal Pricing vs External Pricing

As Bancor's Liquidity Pools do not involve any oracles linking them to the external world, they by design rely on the mechanism of arbitrage to remain in sync with other markets. 

However, for arbitrage to work, there must exist another (in this case: external) market, so that the arbitrager has some context to trade against. Thus, the following question arises: is the Bancor mechanism inherently dependent on the existence of external exchanges?

Luckily for Bancor the answer here is negative. It turns out that arbitrage is only needed when there *are* external markets in existence, as otherwise the need for arbitrage just disappears:

* If at least one external market exists, a Liquidity Pool is just one of multiple sources of truth regarding the price of an asset. Thus, it needs arbitrage activity to keep in sync with them. In this case arbitrage can happen (as there is an external market) and it does happen.
* If external markets do not exist, a Liquidity Pool becomes the only source of truth as far as the price of an asset is concerned. In this case arbitrage cannot happen (as there is no external market for the arbitrager to trade against) but fortunately it is not needed, as there is no other source of truth the Liquidity Pool can be out of sync with.

It's worth noting that Bancor V2 introduces the notion of an oracle as a means of enhancing the efficiency of a Liquidity Pool, but it does so for completely different reasons than those discussed above.

#### Liquidity Pools vs Decentralized Investment Funds

In its primary functionality, a Liquidity Pool is a permissionless mechanism for bootstrapping liquidity, so that anyone can contribute towards a common goal of making an asset liquid, and then participate in the generated income on a fair and transparent basis.

However, while providing liquidity for an asset (or an array of assets), a Liquidity Pool simultaneously acts as an permissionless investment vehicle, or a decentralized investment fund, especially when the reserve weights diverge from the 50/50 default value, or when a Liquidity Pool consists of more than two assets.

This is an interesting side-product of Bancor's liquidity mechanism. Although this feature was not mentioned in the WP, in our view it might become the foundation of more advanced DeFi products in the future.

#### Predicted Price vs Actual Price

`Verification that the price predicted by the Bancor algorithm matches the actual price achieved during the subsequent trade.`

# The Planned Bancor V2 Upgrade

### Three Problems Limiting Bancor's Adoption

The following three issues have been identified as major obstacles in the daily usage of Bancor's Liquidity Pools. 

#### A. Impermanent Loss

Liquidity providers earn trading fees, but in many cases they will suffer an *impermanent loss*, i.e. a loss that diminishes the value of their staked liquidity.

This happens when the relative prices of two tokens change, e.g. when ETH’s price goes up relative to DAI, that essentially gives an opportunity for arbitrageurs to balance the pool, but at the same time causes impermanent loss for liquidity providers.

As a result, those liquidity providers might end up withdrawing less money than they put in, and this is only due to price volatility during a given period, not the market prices shifting to a new level.

#### B. Necessity to Use BNT

Another issue that has limited Bancor’s adoption was the necessity to acquire large amounts of BNT in order to be able to provide liquidity for a token that was otherwise unrelated to BNT. 

As a result, many users were faced with the following dilemma: they wanted their token to be liquid and were willing to stake this token to provide liquidity via a Liquidity Pool but they didn't want to lose their long position in their token, so they don't necessarily want to convert some of those tokens to BNT. 

#### C. Excessive Slippage

Excessive slippage might occur when the volume of a trade is relatively large when compared to the depth of the reserves in a Liquidity Pool. 

Up till now Bancor has been addressing this issue by adding an amplification coefficient. This reduces the amount of slippage relative to the total value in the liquidity pool. This approach comes with its own risks, however, as it can result in the liquidity pool being drained completely.

### The Solution Offered in V2

Bancor V2 offers a solution to all these three problems in the form of *Pegged Reserve Pools* utilizing a price oracle provided by an external vendor (the initial choice is [Chainlink](https://chain.link/)). The oracle can be described as a crutch for Bancor to lean on when balancing the relative liquidity between different tokens.

As Nate Hindman, Bancor's head of growth, explains:

> It's allowing Bancor to build these pegged reserve pools where the relative reserve values are not changed.

In these pegged pools, each conversion will trigger an oracle call. These will balance the liquidity pools according to the relative contribution from each user. This is how Nate Hindman sums up the upgrade's benefits:

> We expect and we hope that this [V2 upgrade] will bring tons more liquidity to the protocol. And that we won't have to have these conversations about impermanent loss or providing liquidity and also holding another token in addition.

Source: [cointelegraph.com](https://cointelegraph.com/news/bancors-upcoming-v2-upgrade-to-solve-defis-dirty-little-secret)
